# HMS PAWN #



### What is HMS PAWN? ###

**HMS PAWN** (**P**ostman **A**PI **W**orkspace **N**exus) is a public workspace consisting of **HMS** (**H**uawei **M**obile **S**ervices) APIs, SDKs, documentation, and web apps.